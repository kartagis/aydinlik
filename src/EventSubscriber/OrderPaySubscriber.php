<?php

namespace Drupal\aydinlik\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Assigns the proper subscription when an order is placed.
 */
class OrderPaySubscriber implements EventSubscriberInterface {

  /**
   * Current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $current_user;

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * NodeAccessSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $this->current_user
   *   Current user account.
   */
  public function __construct(AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->current_user = $this->current_user;
    $this->entity_type_manager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.post_transition' => ['addSubscription', -100],
      'commerce_order.place.pre_transition' => ['removeSubscription', -100]
    ];
    return $events;
  }

  /**
   * Adds subscription upon successful payment.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function addSubscription(WorkflowTransitionEvent $event) {
    $dateTime = \DateTime::createFromFormat('d-m-Y',date('d-m-Y'));
    $today = $dateTime->format('d-m-Y');
    $this->current_user = User::load(\Drupal::currentUser()->id());
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $order_items = $order->getItems();
    $order_item = reset($order_items);
    $product_variation = $order_item->getPurchasedEntity();
    $sku = $product_variation->getSku();
    $from = ["aylik", "yillik", "-"];
    $to = ["Aylık", "Yıllık", " "];
    $name = ucwords(str_replace($from, $to, $sku));
    $epaper_subscription = $this->entity_type_manager->getStorage('taxonomy_term')->loadByProperties(['name' => 'E-Gazete Aboneliği']);
    $subscription_vocab = $this->entity_type_manager->getStorage('commerce_product_variation')->loadByProperties(['sku' => $name]);
    $sku = end($subscription_vocab)->field_sku->value;
    switch ($sku) {
      case 'aylik-abonelik':
      case 'aylik-abonelik-ogrenci':
        if (!empty($this->current_user->field_abonelik_turu)) {
          unset($this->current_user->field_abonelik_turu);
        }
        if ($this->current_user->field_abonelik_baslangic_tarihi->value == null) {
          $this->current_user->field_abonelik_baslangic_tarihi->value = $today;
        }
        $this->current_user->field_abonelik_bitis_tarihi->value = date('d-m-Y', strtotime('+1 month'));
        $this->current_user->field_abonelik_turu[] = ['target_id' => reset($epaper_subscription)->id()];
        $this->current_user->save();
        break;
      case 'yillik-abonelik':
      case 'yillik-abonelik-ogrenci':
        if (!empty($this->current_user->field_abonelik_turu)) {
          unset($this->current_user->field_abonelik_turu);
        }
        if ($this->current_user->field_abonelik_baslangic_tarihi->value == null) {
          $this->current_user->field_abonelik_baslangic_tarihi->value = $today;
        }
        $this->current_user->field_abonelik_bitis_tarihi->value = date('Y-m-d', strtotime('+1 year'));
        $earchive_subscription = $this->entity_type_manager->getStorage('taxonomy_term')->loadByProperties(['name' => 'E-Arşiv Aboneliği']);
        $this->current_user->field_abonelik_turu[] = ['target_id' => reset($epaper_subscription)->id()];
        $this->current_user->field_abonelik_turu[] = ['target_id' => reset($earchive_subscription)->id()];
        $this->current_user->save();
      default:
        # code...
        break;
    }
  }

  /*
   * Removes subscription upon pending payment.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function removeSubscription(WorkflowTransitionEvent $event) {
    $this->current_user = User::load(\Drupal::currentUser()->id());
    if ($this->current_user->hasRole('abone')) {
      $this->current_user->removeRole('abone');
      $this->current_user->save();
    }
  }

}
